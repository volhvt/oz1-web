<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_items',function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('eid');
            $table->unsignedInteger('shop_category_id')->nullable(true);
            //$table->foreign('shop_category_id')->references('id')->on('shop_categories');
            $table->string('url')->nullable(false);
            $table->string('name',312)->nullable(false);
            $table->text('content')->nullable(true);
            $table->text('description')->nullable(true);
            $table->string('currency_code',4)->default(NULL);
            //$table->foreign('currency_code')->references('code')->on('currencies');
            $table->unsignedDecimal('price',8,2)->default(0);
            $table->unsignedDecimal('price_old',8,2)->default(0);
            $table->decimal('discount',8,2)->default(0);
            $table->decimal('discount_percent',8,2)->default(0);
            $table->unsignedDecimal('weight',8,2)->default(0);
            $table->boolean('store')->unsigned()->default(false);
            $table->boolean('pickup')->unsigned()->default(false);
            $table->boolean('delivery')->unsigned()->default(false);
            $table->boolean('active')->unsigned()->default(false);
            $table->boolean('newest')->unsigned()->default(false);
            $table->boolean('manufacturer_warranty')->unsigned()->default(false);
            $table->string('picture', 65)->nullable(true);
            $table->timestamps();

            $table->index(['eid'],'sch_out');
            $table->index(['shop_category_id', 'eid'],'sch_out_by_category');

            $table->index(['shop_category_id', 'price', 'active'],'shc_price_by_category');
            $table->index(['shop_category_id', 'discount', 'active'],'shc_discount_by_category');
            $table->index(['shop_category_id', 'discount_percent', 'active'],'shc_discount_percent_by_category');

            $table->index(['price', 'active'], 'shc_price');
            $table->index(['discount', 'active'], 'shc_discount');
            $table->index(['discount_percent', 'active'], 'shc_discount_percent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_items');
    }
}
