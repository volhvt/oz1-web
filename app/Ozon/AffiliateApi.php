<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 22.07.19
 * Time: 0:53
 */

namespace App\Ozon;


use App\CurlLoader;
use Illuminate\Support\Facades\Storage;

use DateTime;
use ZipArchive;

class AffiliateApi
{
    /**
     * @var string
     */
    private $_url = 'https://api.ozon.ru';

    /**
     * @var string
     */
    private $_email = '';
    /**
     * @var string
     */
    private $_password = '';
    /**
     * @var CurlLoader
     */
    private $_sender;
    /**
     * @var string
     */
    private $_tokenPath;
    /**
     * @var string
     */
    private $_token;
    /**
     * @var
     */
    private $_refreshToken;
    /**
     * @var float|int
     */
    private $_tokenTTL = 30 * 24 * 60 * 60;
    /**
     * @var float|int
     */
    private $_refreshTokenTTL = 90 * 24 * 60 * 60;

    /**
     * AffiliateApi constructor.
     * @param $email
     * @param $password
     * @param $tokenPath
     */
    public function __construct($email, $password, $tokenPath)
    {
        $this->_sender = new CurlLoader();
        $this->_email = $email;
        $this->_password = $password;
        $this->_tokenPath = $tokenPath;
    }

    /**
     * @return string
     */
    public function tokenPath(): string
    {
        return $this->_tokenPath;
    }

    /**
     * @param $email
     * @return $this
     */
    public function email($email)
    {
        $this->_email = $email;
        return $this;
    }

    public function password($password)
    {
        $this->_password = $password;
        return $this;
    }

    public function getToken()
    {
        /*
        1
        POST /affiliates/partner-api/account/token HTTP/1.1
        Host: https://api.ozon.ru
        Content-Type: application/x-www-form-urlencoded
        email={Ваша_эл._почта}&password={Ваш_пароль}
        */

        $this->_sender->loadByCurl(
            $this->_url . '/affiliates/partner-api/account/token',
            [
                'email' => $this->_email,
                'password' => $this->_password
            ],
            CurlLoader::MTPOST,
            [
                'Content-Type: application/x-www-form-urlencoded'
            ]
        );

        if ($this->_sender->hasErrors()) {
            var_dump($this->_sender->errors);
            return;
        }

        $data = $this->_sender->getData();

        var_dump($data);

        $token = json_decode($data, true);
        if (empty($token)) {
            var_dump('!!!Token load error!!!');
            return;
        }
        $this->saveToken($token);
    }

    public function refreshToken()
    {
        /*
        refresh
        PUT /affiliates/partner-api/account/token HTTP/1.1
        Host: https://api.ozon.ru
        Content-Type: application/x-www-form-urlencoded
        refresh_token={Полученный_ранее_рефреш_токен}
         */

        $this->_sender->loadByCurl(
            $this->_url . '/affiliates/partner-api/account/token',
            [
                'refresh_token' => $this->_token['refresh_token'],
            ],
            CurlLoader::MTPUT,
            [
                'Content-Type: application/x-www-form-urlencoded'
            ]
        );

        if ($this->_sender->hasErrors()) {
            var_dump($this->_sender->errors);
            return;
        }

        $data = $this->_sender->getData();
        var_dump($data);

        $token = json_decode($data, true);
        if (empty($token)) {
            var_dump('!!!Token load error!!!');
            return;
        }

        $this->saveToken($token);
    }

    /**
     * @param $path
     */
    public function loadCategories($path)
    {
        //
        $token = $this->accessToken();

        $list = [];
        if (!empty($token)) {
            $this->_sender->loadByCurl(
                $this->_url . '/partner-tools.affiliates/xmlFeed/catalogs.json?token=' . $token,
                [
                    //'token' => $token['access_token'],
                ],
                CurlLoader::MTGET/*,
                [
                    'Content-Type: application/x-www-form-urlencoded'
                ]*/
            );

            if ($this->_sender->hasErrors()) {
                var_dump($this->_sender->errors);
                return;
            }

            $list = json_decode($this->_sender->getData(), true);
        }

        if (!empty($list) && !empty($token)) {
            foreach ($list as $item) {
                if (empty($item['ParentId'])) {
                    self::downloadCategory($item['Name'], $item['Url'] . '?token=' . $token, $path);
                    self::unzip($item['Name'], $path);
                }
            }
        }
    }

    public function token(): array
    {
        $this->_token = [];
        if (is_file($this->tokenPath())) {
            $this->_token = json_decode(file_get_contents($this->tokenPath()), true);
        }

        if (!empty($this->_token) && !empty($this->_token['.expires']) && !empty($this->_token['refresh_token'])
            && new DateTime() >= new DateTime(
                date(
                    'Y-m-d H:i:s',
                    strtotime($this->_token['.expires'] . ' 00:00:00')
                )
            )
        ) {
            $this->getToken();
        }

        if (empty($this->_token) || empty($this->_token['.expires'])
            || new DateTime() >= new DateTime(
                date(
                    'Y-m-d H:i:s',
                    strtotime($this->_token['.expires'] . ' 00:00:00') + $this->_refreshTokenTTL
                )
            )
        ) {
            $this->refreshToken();
        }

        return $this->_token;
    }

    public function accessToken(): string
    {
        $this->token();
        if (!empty($this->_token) && !empty($this->_token['access_token'])) {
            return $this->_token['access_token'];
        }
        return '';
    }

    public function saveToken($token)
    {
        $this->_token = $token;
        file_put_contents($this->tokenPath(), json_encode($this->_token));
    }

    static public function downloadCategory($name, $url, $path)
    {
        echo $name . ' : ' . $url . "\n";
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $file = $path . $name . '.zip';
        @copy($url, $file);
    }

    static public function unzip($name, $path)
    {
        $file = $path . $name . '.zip';
        if (!is_file($file)) {
            return;
        }

        $zip = new ZipArchive();
        if ($zip->open($file) === TRUE) {
            $zip->extractTo($path);
            $zip->close();
        } else {

        }

        unlink($file);
    }
}