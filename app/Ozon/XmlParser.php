<?php
/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 21.07.19
 * Time: 23:17
 */

namespace App\Ozon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;

use Intervention\Image\ImageManagerStatic as Image;

use App\Currency;
use App\ShopCategory;
use App\ShopItem;

use Monolog\Logger;
use SimpleXMLElement;
use XMLReader;

class XmlParser
{
    const MAX_DOWNLOAD_ATTEMPTS = 10;

    /**
     * @var array
     */
    private $_categories = [];
    /**
     * @var array
     */
    private $_categoryTree = [];
    /**
     * @var array
     */
    private $_categoriesLinks = [];
    /**
     * @var int
     */
    private $_defaultShopCategoryId = null;
    /**
     * @var int
     */
    private $_countCategories = 0;
    /**
     * @var int
     */
    private $_countItems = 0;
    /**
     * @var int
     */
    private $_from = 0;
    /**
     * @var int
     */
    private $_to = 0;
    /**
     * @var string
     */
    private $_file = '';
    /**
     * @var XMLReader
     */
    private $_xml = null;
    /**
     * @var
     */
    private $_fileName;
    /**
     * @var int
     */
    private $_countOffers = 0;
    /**
     * @var int
     */
    private $_offerNumber = 0;
    /**
     * @var int
     */
    private $_queueCapasity = 500;
    /**
     * @var array
     */
    private $_queries = [];

    /**
     * XmlParser constructor.
     * @param $file
     */
    public function __construct($file)
    {
        $this->_file = $file;
        $this->_fileName = pathinfo($this->_file, PATHINFO_FILENAME);
    }

    /**
     * @param $from
     * @return XmlParser
     */
    public function from($from = 0)
    {
        $from = intval($from);
        if ($this->_to && $from && $from > $this->_to) {
            throw new \InvalidArgumentException(' from[' . $from . '] must be great then to[' . $this->_to . ']');
        }
        $this->_from = $from;
        return $this;
    }

    /**
     * @param $to
     * @return XmlParser
     */
    public function to($to = 0)
    {
        $to = intval($to);
        if ($this->_from && $to && $to < $this->_from) {
            throw new \InvalidArgumentException(' to[' . $to . '] must be less then from[' . $this->_from . ']');
        }
        $this->_to = $to;
        return $this;
    }

    public static function parseInDir($path, $maxQueues = null, $maxCount = null)
    {
        if (!is_dir($path)) {
            return 404;
        }

        $arFiles = glob($path . DIRECTORY_SEPARATOR . '/*.xml');

        // парсить отдельно в очереди
        if ($maxQueues || $maxCount) {
            $queueNum = 0;
            foreach ($arFiles as $file) {
                $countOffers = 0;
                $parser = new XmlParser($file);

                // подсчет элементов в файле в том случае если указано максимальное кол-во элементов
                if ($maxCount) {
                    // получаем кол-во элементов
                    $countOffers = $parser->getCountXmlElements('offers', 'offer');
                    echo ' =========[ количество товаров: ' . $countOffers . ']========= ' . "\n";
                }

                if ($maxCount && $countOffers > $maxCount) {
                    $from = 0;
                    $to = 0;
                    // разбиваем файл на части и ставим в очередь
                    $countParts = $countOffers / $maxCount;
                    if ($countOffers % $maxCount > 0)
                        $countParts++;
                    for ($i = 0; $i < $countParts; $i++) {
                        $to += $maxCount;
                        Artisan::queue('ozon:parse', ['file' => $file, 'from' => $from, 'to' => $to])
                            ->onConnection('database')->onQueue('parseXml' . sprintf("%d", $queueNum));
                        $from = $to + 1;
                        $queueNum++;
                        if ($maxQueues && $queueNum > $maxQueues) {
                            $queueNum = 0;
                        }
                    }
                } else {
                    // ставим в очередь файл
                    Artisan::queue('ozon:parse', ['file' => $file, 'from' => 0, 'to' => 0])
                        ->onConnection('database')->onQueue('parseXml' . sprintf("%d", $queueNum));
                    $queueNum++;
                }

                if ($maxQueues && $queueNum > $maxQueues) {
                    $queueNum = 0;
                }
            }
            return;
        }

        // парсить файл за файлом
        foreach ($arFiles as $file) {
            $parser = new XmlParser($file);

            // получаем кол-во элементов
            $countOffers = $parser->getCountXmlElements('offers', 'offer');
            echo ' =========[ количество товаров: ' . $countOffers . ']========= ' . "\n";

            $parser
                ->from(0)
                ->to(0)
                ->parse();

            $parser->unlink();
        }

        return 0;
    }

    /**
     * @return int
     */
    public function parse()
    {

        if (!$this->isValidFile()) {
            return 404;
        }

        $totalSize = filesize($this->_file);
        echo ' ---------[ size ' . ($totalSize / 1024) . ' Kb file:' . $this->_file . ' ]-------- ' . "\n";

        // получаем кол-во элементов
        $this->getCountXmlElements('offers', 'offer');
        echo ' =========[ количество товаров: ' . $this->_countOffers . ']========= ' . "\n";

        $this->_xml = new XMLReader();
        if (!$this->_xml->open($this->_file)) {
            return 500;
        }

        while ($this->_xml->read()) {
            switch ($this->_xml->name) {
                case 'currencies':
                    $this->_parseItems('currencies', 'currency', '_updateOrCreateCurrency');
                    break;
                case 'categories':
                    $this->_parseItems('categories', 'category', '_collectShopCategories');
                    $this->_updateOrCreateShopCategories('');
                    if (!empty($this->_categories)) { // !!!!! есть категории у которых родитель отсутствует в списке !!!!!!!!!!
                        foreach ($this->_categories as $categoryId => $parentId) {
                            $this->_updateOrCreateShopCategories($parentId);
                        }
                    }
                    break;
                case 'offers':
                    $this->_parseItems('offers', 'offer', '_updateOrCreateShopItem');
                    break;
            }
        }

        echo 'memory usage ' . (memory_get_usage() / 1024) .
            '(real: ' . (memory_get_usage(true) / 1024) . ")kb\n";
        echo 'memory peak usage ' . (memory_get_peak_usage() / 1024) .
            '(real: ' . (memory_get_peak_usage(true) / 1024) . ")kb\n";
        echo 'memory process usage ' . self::memory_get_process_usage() . "kb\n";

        $this->_xml->close();
        return 0;
    }

    public function getCountXmlElements($groupName, $itemName)
    {
        $this->_countOffers = 0;
        $handler = fopen($this->_file, "r");
        if ($handler) {
            $inGroup = false;
            while (($row = fgets($handler)) !== false) {
                if (preg_match('|<' . $groupName . '|is', $row)) {
                    $inGroup = true;
                } else if (preg_match('|</' . $groupName . '|is', $row)) {
                    break;
                } else if ($inGroup) {
                    $matches = [];
                    if (preg_match_all('|<' . $itemName . '|is', $row, $matches)) {
                        $this->_countOffers += count($matches);
                    }
                }
            }
            fclose($handler);
        }
        return $this->_countOffers;
    }

    /**
     * parse elements in group from Xml object
     *
     * @param $groupName
     * @param $itemName
     * @param $callback
     */
    protected function _parseItems($groupName, $itemName, $callback)
    {
        $this->_offerNumber = 0;
        echo "GROUP <{$this->_xml->name}>| {$this->_xml->nodeType} \n";
        // пустой элемент группы />
        if ($this->_xml->isEmptyElement) {
            return;
        }
        while (!($this->_xml->name == $groupName && $this->_xml->nodeType == XMLReader::END_ELEMENT)) {
            // echo '.';
            if ($this->_xml->name == $itemName && $this->_xml->nodeType == XMLReader::ELEMENT) {
                // echo "  #{$this->_offerNumber}\n";
                // если не товар и задан параметр с какого элемента начинать парсить
                if (
                    $itemName != 'offer'
                    || $this->_from <= 0
                    || ($this->_from <= $this->_offerNumber && ($this->_to <= 0 || $this->_to >= $this->_offerNumber))
                ) {
                    //echo "  ITEM <{$this->_xml->name}>| {$this->_xml->nodeType}\n";
                    $element = new SimpleXMLElement($this->_xml->readOuterXML());
                    if (is_string($callback)) {
                        call_user_func([$this, $callback], $element);
                    } else if (is_callable($callback)) {
                        call_user_func($callback, $element);
                    }
                    unset($element);
                    //echo "  ITEM </{$this->_xml->name}>| {$this->_xml->nodeType}\n";
                }
                $this->_offerNumber++;
            }

            // пустой элемент группы />
            if ($this->_xml->name == $groupName && $this->_xml->isEmptyElement) {
                break;
            }

            $this->_xml->read();
        }
        echo "GROUP </{$this->_xml->name}>| {$this->_xml->nodeType} \n";
    }

    /**
     * update or create currency in DB from Xml object
     *
     * @param SimpleXMLElement $element
     */
    protected function _updateOrCreateCurrency(SimpleXMLElement $element)
    {
        $id = strval($element->attributes()->id);
        $currency = Currency::firstOrCreate(['code' => $id]);
        $currency->rate = doubleval($element->attributes()->rate);
        $currency->save();
    }

    /**
     * add shop category from Xml object to $_categoryTree by parent,
     * add link between id category and his parent to $_categories
     *
     * @param SimpleXMLElement $element
     */
    protected function _collectShopCategories(SimpleXMLElement $element)
    {
        $id = trim(strval($element->attributes()->id));
        $parentId = trim(strval($element->attributes()->parentId));
        if (!$parentId) {
            $this->_defaultShopCategoryId = $id;
        }
        $item = [
            'eid' => $id,
            'parent_id' => $parentId,
            'tid' => NULL,
            'name' => strval($element)
        ];

        if (!isset($this->_categoryTree[$parentId])) {
            $this->_categoryTree[$parentId] = [];
        }
        $this->_categoryTree[$parentId][$id] = $item;
        $this->_categories[$id] = $parentId;
    }

    /**
     * @param $parentId
     */
    protected function _updateOrCreateShopCategories($parentId)
    {
        if (!empty($this->_categoryTree[$parentId])) {
            foreach ($this->_categoryTree[$parentId] as $id => $category) {
                $this->_updateOrCreateShopCategory($category);
            }
        }
    }

    /**
     * update or create shop category in DB from Xml object,
     * add link between real id and external id to $_categoriesLinks
     *
     * @param $category
     */
    protected function _updateOrCreateShopCategory($category)
    {
        echo ' shop category # ' . (++$this->_countCategories) . "\n";
        $shopCategory = ShopCategory::where('eid', $category['eid'])->first();
        if (!$shopCategory) {
            $shopCategory = new ShopCategory();
        }

        if (isset($this->_categoriesLinks[$category['parent_id']])) {
            $category['parent_id'] = $this->_categoriesLinks[$category['parent_id']];
        } else if ($this->_defaultShopCategoryId && isset($this->_categoriesLinks[$this->_defaultShopCategoryId])) {
            $category['parent_id'] = $this->_categoriesLinks[$this->_defaultShopCategoryId];
        } else if (isset($category['parent_id'])) {
            unset($category['parent_id']);
        }
        $shopCategory->fill($category);
        if ($this->_defaultShopCategoryId != null && $category['eid'] == $this->_defaultShopCategoryId) {
            $shopCategory->saveAsRoot();
        } else {
            $shopCategory->save();
        }

        $this->_categoriesLinks[$category['eid']] = $shopCategory->id;
        unset($this->_categories[$category['eid']]);
        $this->_updateOrCreateShopCategories($category['eid']);
    }

    /**
     * update or create shop item entity from Xml object,
     * download pictures for shop item
     *
     * @param SimpleXMLElement $element
     */
    protected function _updateOrCreateShopItem(SimpleXMLElement $element)
    {
        echo $this->_fileName . '[' . $this->_from . ':' . $this->_to . ']>>> shop item # ' . (++$this->_countItems) . ' of ' . $this->_countOffers . "\n";
        $price = doubleval($element->price);
        $priceOld = doubleval($element->oldprice);
        $discount = $discountPercent = 0.0;
        if ($price > 0 && $priceOld > $price) {
            $discount = $priceOld - $price;
            $discountPercent = $discount * 100.0 / $priceOld;
        }
        $id = strval($element->attributes()->id);
        $categoryId = strval($element->categoryId);
        echo "\t\t try search shop item {$id} \n";
        $shopItem = ShopItem::where('eid', $id)->first();
        if (!$shopItem) {
            echo "\t\t NOT FOUND shop item by {$id} \n";
            $shopItem = new ShopItem();
        }

        echo "\t\t try feel shop item {$shopItem->id} \n";
        $shopItem->fill(
            [
                'eid' => $id,
                'shop_category_id' => isset($this->_categoriesLinks[$categoryId]) ? $this->_categoriesLinks[$categoryId] : NULL,
                'url' => strval($element->url),
                'name' => strval($element->name),
                'content' => NULL,
                'description' => substr(strval($element->description), 0, 65535),
                'currency_code' => strval($element->currencyId),
                'price' => $price,
                'price_old' => $priceOld,
                'discount' => $discount,
                'discount_percent' => $discountPercent,
                'weight' => doubleval($element->weight),
                'store' => self::toBool(strval($element->store)),
                'pickup' => self::toBool(strval($element->pickup)),
                'delivery' => self::toBool(strval($element->delivery)),
                'active' => self::toBool(strval($element->attributes()->available)),
                'newest' => self::isNewest($shopItem),
                'manufacturer_warranty' => self::toBool(strval($element->manufacturer_warranty)),
            ]
        );

        if (!empty($element->picture)) {
            if (isset($element->picture[0])) {
                echo "\t\t need download {$element->picture[0]}\n";
                $shopItem->picture = self::_downloadPicture(strval($element->picture[0]), Storage::disk('shop'), $shopItem->picture);
            }
        }

        if (!empty($element->param)) {
            $content = '';
            foreach ($element->param as $param) {
                $content .= '<li><b>' . $param->attributes()->name . '</b>: ' . mb_substr(strval($param), 0, 256, 'UTF-8')
                    . (isset($param->attributes()->unit) ? (' ' . $param->attributes()->unit) : '')
                    . '</li>' . "\n";
            }
            $shopItem->content = '<ul>' . $content . '</ul>';
        }

        try {
            if ($shopItem->save()) {
                echo "\t\t AS {$shopItem->id}\n";
            } else {
                echo "\t\t ERROR !!!\n";
            }
        } catch (\Exception $e) {
            echo "\t\t !!!! " . $e->getMessage() . "\n";
        }

        unset($shopItem);
    }

    /**
     * Returns memory usage from /proc<PID>/status in kbytes.
     *
     * @return int|bool sum of VmRSS and VmSwap in kbytes. On error returns false.
     */
    static public function memory_get_process_usage()
    {
        $status = file_get_contents('/proc/' . getmypid() . '/status');

        $matchArr = array();
        preg_match_all('~^(VmRSS|VmSwap):\s*([0-9]+).*$~im', $status, $matchArr);

        if (!isset($matchArr[2][0]) || !isset($matchArr[2][1])) {
            return false;
        }

        return intval($matchArr[2][0]) + intval($matchArr[2][1]);
    }

    /**
     * @param string $value
     * @return bool
     */
    static public function toBool(string $value): bool
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * @param Model $entity
     * @return bool
     */
    static public function isNewest(Model $entity): bool
    {
        return !empty($entity->created_dt)
            && $entity->created_dt > new \DateTime(date('Y-m-d H:i:s', time() + 10 * 24 * 60 * 60));
    }

    /**
     * @param $url
     * @param \Illuminate\Filesystem\FilesystemAdapter $fs
     * @param $currentPath
     * @return string
     */
    static public function _downloadPicture($url, FilesystemAdapter $fs, $currentPath): string
    {
        echo "\t\t URL: {$url}\n";
        $urlPath = parse_url($url, PHP_URL_PATH);
        $pathParts = pathinfo($urlPath);
        $name = md5($url);
        $path = $fs->path(
            substr($name, 0, 1) . '/' .
            implode('/', str_split(substr($name, 1, 4), 2)) . '/'
        );
        if (!is_dir($path)) {
            @mkdir($path, 0777, true);
        }
        $file = $path . $name . '.' . $pathParts['extension'];

        // нет файла или mime тип файла не картинка
        if (!file_exists($file) || strpos(mime_content_type($file), 'image') === false) {
            echo "\t\t $file\n";
            $attempts = 0;
            // пытаемся закачать N раз
            while (!@copy($url, $file) && ++$attempts < self::MAX_DOWNLOAD_ATTEMPTS) {
                echo '\t\t   ----- ' . $attempts . "\n";
                usleep(500);
            }
            echo "\n";
            // кол-во попыток на закачку исчерпано и есть файл, удалить так как возможно битый
            if ($attempts >= self::MAX_DOWNLOAD_ATTEMPTS && file_exists($file)) {
                unlink($file);
            }

            // есть файл и mime тип файла картинка
            if (file_exists($file) && strpos(mime_content_type($file), 'image') !== false) {
                try {
                    $img = Image::make($file);
                    $img->resize(null, 400, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save($file);
                    if ($currentPath && is_file(public_path() . $currentPath)) {
                        echo '\t\t   - delete current ' . $currentPath . "\n";
                        unlink(public_path() . $currentPath);
                    }
                } catch (\Exception $e) {

                    unlink($file);
                    return $currentPath;
                }
            } else {
                return $currentPath;
            }
        }

        return strtr($file, [public_path() => '']);
    }

    public function isValidFile()
    {
        return strlen(trim($this->_file)) > 0 && is_file($this->_file);
    }

    public function canUnlink()
    {
        return $this->_to == 0 || $this->_to >= $this->_countOffers;
    }

    public function unlink()
    {
        return $this->isValidFile() && unlink($this->_file);
    }
}