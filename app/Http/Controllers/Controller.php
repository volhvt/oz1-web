<?php

namespace App\Http\Controllers;

use App\ShopCategory;
use App\ShopItem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(Request $request)
    {
        /** @var $pagination \Illuminate\Pagination\LengthAwarePaginator */
        /** @var $shopItems  \Illuminate\Database\Eloquent\Builder */
        /** @var $shopItems  \Illuminate\Database\Eloquent\Collection */

        $allowSorts = ['price', 'name', 'discount', 'discount_percent'];
        $sort = in_array($request->sort, $allowSorts) ? $request->sort : 'discount_percent';
        $order = $request->order === 'asc' ? 'asc' : 'desc';
        $limit = $request->limit ? $request->limit : 50;

        $sorter = [
            'selected' => 0,
            /*[
                'title'   => __('layout.Default'),
                'params'  => ['sort'=>'discount','order'=>'desc']
            ],*/
            [
                'title' => __('layout.Discount percentage (Highest)'),
                'params' => ['sort' => 'discount_percent', 'order' => 'desc']
            ],
            [
                'title' => __('layout.Discount percentage (Lowest)'),
                'params' => ['sort' => 'discount_percent', 'order' => 'asc']
            ],
            [
                'title' => __('layout.Discount (Highest)'),
                'params' => ['sort' => 'discount', 'order' => 'desc']
            ],
            [
                'title' => __('layout.Discount (Lowest)'),
                'params' => ['sort' => 'discount', 'order' => 'asc']
            ],
            [
                'title' => __('layout.Price (Low > High)'),
                'params' => ['sort' => 'price', 'order' => 'asc']
            ],
            [
                'title' => __('layout.Price (High > Low)'),
                'params' => ['sort' => 'price', 'order' => 'desc']
            ]
        ];

        $breadcrumbs = null;
        $shopCategory = null;
        // Все категории
        //DB::enableQueryLog();

        $shopCategories = [];//ShopCategory::tree();
        $subShopCategories = [];

        $shopCategoryId = $request->categoryId;

        $shopItems = ShopItem::where('active', 1)->where('discount', '>', 0)->orderBy($sort, $order);

        if ($shopCategoryId) {
            $shopCategory = ShopCategory::findOrFail($shopCategoryId);

            $shopCategoryIds = $shopCategory->descendants()->pluck('id');
            $shopCategoryIds[] = $shopCategory->getKey();

            $shopItems->whereIn('shop_category_id', $shopCategoryIds);

            if (!$request->ajax()) {
                $subShopCategories = $shopCategory->children;
                $breadcrumbs = ShopCategory::ancestorsOf($shopCategoryId);
            }
        } else {
            if (!$request->ajax()) {
                $subShopCategories = ShopCategory::whereNull('parent_id')->get();
            }
        }

        $pagination = $shopItems->paginate($limit);


        foreach ($sorter as $num => $item) {
            if (is_array($item)) {
                $sorter[$num]['params'][$pagination->getPageName()] = $pagination->currentPage();
                $sorter[$num]['params']['perPage'] = $pagination->perPage();
                if ($item['params']['sort'] == $sort && $item['params']['order'] == $order) {
                    $sorter['selected'] = $num;
                }
            }
        }

        //dd(DB::getQueryLog());

        $pagination = $pagination->appends(
            [
                'sort' => $sort,
                'order' => $order,
                'limit' => $limit,
            ]
        );

        $data = [
            'pagination' => $pagination,
            'sorter' => $sorter,
            'items' => $shopItems,
            'category' => $shopCategory,
            'categories' => $shopCategories,
            'subCategories' => $subShopCategories,
            'breadcrumbs' => $breadcrumbs
        ];

        if ($request->ajax()) {
            return view('shop.index', $data);;
        }

        return view('index', $data);
    }

    public function item(Request $request)
    {
        $item = ShopItem::findOrFail($request->itemId);
        return view('shop.item-short', ['item' => $item]);
    }
}
