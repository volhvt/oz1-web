<?php

namespace App;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ShopItem extends Model
{
    //use Cachable;

    protected $cachePrefix = "sis";

    protected $cacheCooldownSeconds = 24 * 60 * 60;

    protected $fillable = [
        'eid', 'shop_category_id', 'url', 'name', 'content', 'description', 'currency_code',
        'currency_code', 'price', 'price_old', 'discount', 'discount_percent', 'weight', 'store', 'pickup', 'delivery',
        'active', 'newest', 'manufacturer_warranty', 'picture'
    ];

    protected  $dates = ['created_dt','updated_dt'];


    public function imageUrl($width=null,$height=null)
    {
        $img = empty($this->picture)?'/image/no-photo-item.png':$this->picture;
        $file = public_path($img);
        if (!is_file($file) || $width==null && $height == null)
            return $img;

        $pathParts = pathinfo(parse_url($img, PHP_URL_PATH));
        $resizePath = Storage::disk('cache')->path($pathParts['dirname'] . '/');
        //$resizeFile = $resizePath . ($width?$width:'null') . '_' . ($height?$height:'null') . '_' . $pathParts['basename'];
        $resizeFile = $resizePath . ($width?$width:'null') . '_' . ($height?$height:'null') . '_' . $pathParts['filename'].'.webp';
        if (!is_dir($resizePath)) {
            @mkdir($resizePath, 0777, true);
        }
        if (!is_file($resizeFile)) {
            $img = Image::make($file)->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($resizeFile);
        }
        return Storage::disk('cache')->url(strtr($resizeFile,[Storage::disk('cache')->path('')=>'']));
    }

    public function getImageUrlH400Attribute()
    {
        return $this->imageUrl(null,400);
    }

    public function getImageUrlH200Attribute()
    {
        return $this->imageUrl(null,200);
    }
}
