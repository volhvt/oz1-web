<?php
/*

currencies
    <currency id="RUR" rate="1" />

categories
    <category id="6500">Красота и здоровье</category>

offers
    <url>https://www.ozon.ru/context/detail/id/5501407/?utm_content=partner_xml</url>
    <price>679</price>
    <oldprice>900</oldprice>
    <currencyId>RUR</currencyId>
    <categoryId>6510</categoryId>
    <picture>https://cdn1.ozone.ru/multimedia/1031626240.jpg</picture>
    <picture>https://cdn1.ozone.ru/multimedia/1031626241.jpg</picture>
    <picture>https://cdn1.ozone.ru/multimedia/1031626249.jpg</picture>
    <picture>https://cdn1.ozone.ru/multimedia/1031626242.jpg</picture>
    <picture>https://cdn1.ozone.ru/multimedia/1031626243.jpg</picture>
    <picture>https://cdn1.ozone.ru/multimedia/1031626245.jpg</picture>
    <picture>https://cdn1.ozone.ru/multimedia/1031626246.jpg</picture>
    <picture>https://cdn1.ozone.ru/multimedia/1031626248.jpg</picture>
    <store>false</store>
    <pickup>true</pickup>
    <delivery>true</delivery>
    <sales_notes>Быстрая доставка. Простой возврат до 60 дней.</sales_notes>
    <name>Max Factor Тональный крем "Colour Adapt", тон 70 Natural (Натуральный), 34 мл</name>
    <vendor>Max Factor</vendor>
    <vendorCode>80957267</vendorCode>
    <description>Тональная основа, которая поможет справиться с неровным тоном лица. Тональная основа Max Factor Colour Adapt адаптируется к тону кожи и создает естественное покрытие. Так в чем же секрет? «Умные частицы» тональной основы адаптируются к тону кожи, который отличается в разных зонах, обеспечивая невероятно легкое покрытие и не маскируя естественное свечение кожи. Тональная основа Max Factor Colour Adapt обеспечивает идеальный тон, кожа выглядит свежей и сияющей весь день. Инновационная технология для совершенства кожи. Помогает выровнять тон кожи, скрыть несовершенства и мелкие морщинки для создания идеального образа. Тональная основа Max Factor Colour Adapt имеет легкую формулу крем-пудры и подстраивается под любой тон кожи для удивительно естественного образа. Эмульсия на основе силикона и эластомера адаптируется к тону кожи и обеспечивает естественное умеренное покрытие для создания нежного и невесомого макияжа. Основа не содержит масел, подходит для чувствительной кожи, протестирована дерматологами, не закупоривает поры. Кремово-пудровая текстура для безупречного покрытия. Ультралегкая формула не сушит кожу. Содержит несколько цветовых пигментов, которые адаптируются к тону кожи. Не содержит масла, подходит для чувствительной кожи. Не закупоривает поры. Протестировано дерматологами. Сравни тон основы с тоном кожи на щеках и линии челюсти, чтобы подобрать идеальный оттенок. Если тон подобран правильно, то он буквально исчезнет на твоей коже. Чтобы достичь ровного покрытия, на... Рекомендуем!</description>
    <manufacturer_warranty>false</manufacturer_warranty>
    <country_of_origin>Ирландия</country_of_origin>
    <barcode>5011321103993</barcode>
    <weight>0.055</weight>
    <param name="Вес" unit="г">55</param>
    <dimensions>10.000/5.000/3.000</dimensions>
    <param name="Ширина упаковки" unit="мм">50</param>
    <param name="Высота упаковки" unit="мм">30</param>
    <param name="Глубина упаковки" unit="мм">100</param>
    <param name="Тип">Тональный крем</param>
    <param name="Тип кожи">Для всех типов кожи</param>
    <param name="Текстура">Кремообразная</param>
    <param name="Способ применения">Нанести на лицо, равномерно распределить.</param>
    <param name="Особенности состава">Проверено дерматологами</param>
    <param name="Объем" unit="мл">34</param>
    <param name="Состав">Циклометикон, вода, глицерин, двуокись титана, диметикон сополимер, слюда, диметикон, ПЕГ – 10 диметикон сополимер, диметилциклосилоксан, метикон, ПЕГ/ППГ – 18/ 18 метикон, бензиловый спирт, этилпарабен, двунатриевая этилендиаминтетрауксусная кислота, метилпарабен, пропилпарабен,</param>
 */

namespace App\Console\Commands;


use App\Ozon\XmlParser;

use Illuminate\Console\Command;

class ParseOzonXml extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ozon:parse {file} {from=0} {to=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse one ozon xml file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parser = new XmlParser($this->argument('file'));
        $from = $this->argument('from', 0);
        $to = $this->argument('to', 0);
        $status = $parser
            ->from($from)
            ->to($to)
            ->parse();

        if ($parser->canUnlink())
            $parser->unlink();

        return $status;
    }
}
