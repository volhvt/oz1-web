<?php

namespace App\Console\Commands;

use App\Ozon\AffiliateApi;
use App\Ozon\XmlParser;
use Encore\Admin\Config\Config;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class LoadOzonXml extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ozon:load {max-queues=7} {split=100000}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load ozon affiliate catalog and then parse all xml files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = Storage::disk('shop')->path('xml');
        $maxQueues = $this->argument('max-queues', 7);
        $splitCount = $this->argument('split', 100000);
        $api = new AffiliateApi(
            config('partner_api_login'),//'opt@ryukzaki-diskont.ru',
            config('partner_api_password'),//'1q2w3E4r',
            Storage::disk('shop')->path('ozon-token.json')
        );
        $api->loadCategories($path);
        XmlParser::parseInDir($path, $maxQueues, $splitCount);
        return 0;
    }

}
