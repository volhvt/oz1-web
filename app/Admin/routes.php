<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->get('/currency', 'CurrencyController@index')->name('admin.currency');
    $router->get('/currency/{id}', 'CurrencyController@datail')->name('admin.currency.view');
    $router->get('/currency/{id}/edit', 'CurrencyController@form')->name('admin.currency.edit');

});
