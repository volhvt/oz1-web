<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Kalnoy\Nestedset\NodeTrait;

use Illuminate\Database\Eloquent\Model;

class ShopCategory extends Model
{
    use NodeTrait;

    const TREE_DEPTH = 2;
    const CACHE_TREE_KEY = 'shop_category_tree_s_depth_' . self::TREE_DEPTH;

    protected $fillable = ['name', 'eid', 'tid', 'parent_id'];

    protected  $dates = ['created_dt','updated_dt'];

    public function items()
    {
        return $this->hasMany(ShopItem::class, 'shop_category_id');
    }

    public function itemsCount()
    {
        return $this->hasOne(ShopItem::class)
        ->selectRaw('shop_category_id, count(*) as aggregate')
            ->groupBy('shop_category_id');
    }

    public function getItemsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!array_key_exists('ItemsCount', $this->relations))
            $this->load('ItemsCount');

        $related = $this->getRelation('ItemsCount');

        // then return the count directly
        return ($related) ? (int) $related->aggregate : 0;
    }

    public function getItemsCountWithSubAttribute()
    {
        // Get ids of descendants
        $categories = $this->descendants()->pluck('id');
        // Include the id of category itself
        $categories[] = $this->getKey();

        return ShopItem::whereIn('shop_category_id', $categories)->count();
    }

    /**
     * @return []
     */
    static public function tree()
    {
        $tree = unserialize(Cache::store('file')->get(self::CACHE_TREE_KEY));
        if (!$tree) {
            $tree = ShopCategory::withDepth()->having('depth', '<', self::TREE_DEPTH)->get()->toTree();
            $expiresAt = Carbon::createFromFormat('Y-m-d',date('Y-m-d'))->addDay(1);
            Cache::store('file')->put(self::CACHE_TREE_KEY, serialize($tree), $expiresAt);
        }
        return $tree;
    }

}
