<?php
namespace App;

class CurlLoader
{
    const MTGET = 'GET';
    const MTPOST = 'POST';
    const MTPUT = 'PUT';

    const REQUEST_URI_LOGIN = 'login/login';
    const COOKIES_FILE_NAME = 'cookies.txt';

    public $username;
    public $password;

    public $useListAgents = false;
    public $defaultUserAgent = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/45.0.2454.101 Chrome/45.0.2454.101 Safari/537.36';

    public $useListIps = false;
    public $defaultUserIp = '';

    public $useListProxies = false;
    public $defaultProxy = [];

    public $errors = [];

    private $_data = null;

    protected $_use_cache = false;
    protected $_cash_ttl = 30;//sec

    public function __construct()
    {
        $this->_defaultLogCategory = 'curl';
    }

    public function getUserAgent()
    {
        $agentName = $this->defaultUserAgent;
        $file = '/lists/user_agent.json';
        if( $this->useListAgents && file_exists($file) ) {
            $agentName = json_decode(file_get_contents($file),true);
        }

        if( is_array( $agentName ) ) {
            $idx = rand(0, count($agentName) - 1);
            $agentName = $agentName[$idx];
        }

        return $agentName;
    }

    public function getIp()
    {
        $userIp = $this->defaultUserIp;
        $file = '/lists/ip.json';
        if( $this->useListIps && file_exists($file) ) {
            $userIp = json_decode(file_get_contents($file),true);
        }

        if( is_array( $userIp ) ) {
            $idx = rand(0, count($userIp) - 1);
            $userIp = $userIp[$idx];
        }
        return $userIp;
    }

    public function getProxy()
    {
        $proxy = $this->defaultProxy;
        $file = '/lists/proxy.json';
        if( $this->useListProxies && file_exists($file) ) {
            $proxy = json_decode(file_get_contents($file),true);
        }

        if( is_array( $proxy ) && !empty( $proxy ) ) {
            $idx = rand(0, count($proxy) - 1);
            $proxy = $proxy[$idx];
        }
        return $proxy;
    }

    /**
     * @return boolean
     */
    protected function _cache($idx)
    {
        if( $this->_use_cache ) {
            // TODO
            $value = false;
            if ($value !== false) {
                $this->_data = $value;
                return true;
            }
        }
        return false;
    }

    /**
     *
     */
    protected function _toCache($idx)
    {
        if( $this->_use_cache ) {
            // TODO
            return true;
        }
        return false;
    }

    static public function generateIndex($ar,$arExplode=[])
    {
        $idx = '';
        if( is_array($ar) ){
            foreach( $ar as $key => $component ){
                $checked = true;
                if( !empty($arExplode) && !is_numeric($key)
                    && ( isset( $arExplode[$key] ) || in_array($key,$arExplode) )
                ){
                    $checked = false;
                }

                if( $checked ) {
                    if (is_array($component) || is_object($component)) {
                        $idx .= self::getPrintrStr($component);
                    } else {
                        $idx .= $component;
                    }
                }
            }
        } else {
            $idx = $ar;
        }
        return $idx?md5($idx):$idx;
    }

    /**
     * @param string $url
     * @param array | string $data
     * @param string $method
     * @param array $headers
     * @param bool $isAjax
     * @param string $agentName
     * @param string $referrer
     * @param string $ip
     * @param array $proxy
     * @return $this
     */
    function &loadByCurl($url, $data = "", $method = self::MTGET, $headers=[], $isAjax = false, $agentName='auto', $referrer='', $ip = '', $proxy = [] )
    {
        /*$this->log(' START send '."\n".
            ' url : '.$url."\n".
            ' '.$method.'      : '.Logger::getPrintrStr($data)."\n".
            ' headers   : '.Logger::getPrintrStr($headers)."\n".
            ' isAjax    : '.$isAjax."\n".
            ' agentName : '.$agentName."\n".
            ' referrer  : '.$referrer."\n".
            ' ip        : '.$ip."\n".
            ' proxy     : '.Logger::getPrintrStr($proxy)."\n"
        );*/
        $this->clear();

        $idx = self::generateIndex(func_get_args());

        /*if( $this->_cache($idx) ){
            $this->log('not send request used cache');
            return $this;
        }*/

        if( $method == self::MTGET && !empty($data) ) {
            $url = trim($url,'/\?').'/?'.(is_array($data)?http_build_query($data):$data);
        }

        //$this->log('to url '.$url."\n");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25);

        if ($isAjax) {
            $headers[] = 'X-Requested-With: XMLHttpRequest';
        }

        if ($referrer){
            curl_setopt($ch, CURLOPT_REFERER, $referrer);
        }

        if( !empty($headers) && is_array($headers) ) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        if( $method == self::MTPOST ) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, is_array($data)?http_build_query( $data ):$data );
        }

        if( !empty( $agentName ) ) {
            if( $agentName == 'auto' ){
                $agentName = $this->getUserAgent();
            }
            curl_setopt($ch, CURLOPT_USERAGENT, $agentName);
        }



        if(  !empty( $ip ) ) {
            curl_setopt($ch, CURLOPT_INTERFACE, $ip);
        } else if( !empty( $proxy ) && is_array( $proxy ) ) {
            curl_setopt($ch, CURLOPT_PROXY, $proxy['host'] . ':' . $proxy['port']);
            if ($proxy['login'] && $proxy['password']) {
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxy['login'] . ':' . $proxy['password']);
            }
        }

        $this->_data = curl_exec($ch);

        $status = curl_getinfo($ch,CURLINFO_HTTP_CODE);

        if( curl_errno($ch) !== 0 ) {
            $this->errors[] = curl_error($ch);
        }

        if( $status !== 200 ) {
            $this->errors[] = 'End load with HTTP code: '.$status;
            //$this->errorLog(implode("\n",$this->errors));
        } else {

            /*$this->log('End load with '."\n".
                ' DATA      : '.Logger::getPrintrStr($this->_data,300)
            );*/

            /*
             $this->_toCache($idx);
            */
        }

        curl_close($ch);

        return $this;
    }

    public function &cache($timeout=30)
    {
        $this->_cash_ttl  = $timeout;
        $this->_use_cache = true;
        return $this;
    }

    public function &setCacheTimeout($timeout=30)
    {
        $this->_cash_ttl = $timeout;
        return $this;
    }

    public function &setCache($tune=false)
    {
        $this->_use_cache = $tune;
        return $this;
    }

    public function getData()
    {
        return $this->_data;
    }

    public function &clear()
    {
        $this->_data = null;
        $this->errors = [];
        return $this;
    }

    public function hasErrors()
    {
        return !empty($this->errors);
    }

    static public function getPrintrStr($component,$maxLen=0)
    {
        return $maxLen
            ? mb_substr(strtr(print_r($component,true),["\n"=>'']),0,$maxLen,'UTF-8').' ...'
            : strtr(print_r($component,true),["\n"=>'']);
    }

}