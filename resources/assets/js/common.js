function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

$(document).ready(function () {
    // Highlight any found errors
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });

    /* Search */
    $('#search input[name=\'search\']').parent().find('button').on('click', function () {
        var url = $('base').attr('href');

        var value = $('header #search input[name=\'search\']').val();

        if (value) {
            url += encodeURIComponent(value);
        }

        location = url;
    });

    $('#search input[name=\'search\']').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('header #search input[name=\'search\']').parent().find('button').trigger('click');
        }
    });

    // Menu
    $('#menu .dropdown-menu').each(function () {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();

        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 10) + 'px');
        }
    });

    // Product List
    $('#list-view').click(function () {
        $('#content .product-grid > .clearfix').remove();
        $('.product-list-js').fadeOut(500, function () {
            $('#content .row > .product-grid').attr('class', 'product-layout product-list');
            $(this).find('.image').addClass('col-xs-12 col-sm-6 col-md-4 col-lg-3');
            $(this).find('.caption').addClass('col-xs-12 col-sm-6 col-md-8 col-lg-9');
        });
        $('#grid-view').removeClass('active');
        $('#list-view').addClass('active');
        localStorage.setItem('display', 'list');
        $('.product-list-js').fadeIn(500);

    });

    // Product Grid
    $('#grid-view').click(function () {
        // What a shame bootstrap does not take into account dynamically loaded columns
        var cols = $('#column-right, #column-left').length;

        $('.product-list-js').fadeOut(0, function () {

            $('.image').removeClass('col-xs-12 col-sm-6 col-md-4 col-lg-3');
            $('.caption').removeClass('col-xs-12 col-sm-6 col-md-8 col-lg-9');
            if (cols == 2) {
                $('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-6 col-xs-12');
            } else if (cols == 1) {
                $('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-4 col-sm-4 col-xs-12');
            } else {
                $('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-4 col-sm-6 col-xs-12');
            }
        });
        $('#list-view').removeClass('active');
        $('#grid-view').addClass('active');
        localStorage.setItem('display', 'grid');
        $('.product-list-js').fadeIn(500);
    });

    if (localStorage.getItem('display') == 'list') {
        $('#list-view').trigger('click');
        $('#list-view').addClass('active');
    } else {
        $('#grid-view').trigger('click');
        $('#grid-view').addClass('active');
    }

    // tooltips on hover
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body', trigger: 'hover'});

    // Makes tooltips work on ajax generated content
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body', trigger: 'hover'});
    });

    // ajax load on scroll
    var $shopItemsList = $('.products .product-list-js');
    var $shopItemsPaginationInfo = $('.products .category-pagination');
    var $shopPagination = $('.products .pagination');
    var isLoading = false;
    $(document).scroll(function (event) {
        /*console.log(
            isLoading + ' >>>> '
            + $(document).scrollTop() + ' X ' + $(document).scrollLeft()
            + ' | w:' + $(window).height()
            + ' | vw:' + ($(document).scrollTop() + $(window).height())
            + ' | items:' + (parseInt($shopItemsList.height()) + parseInt($shopItemsList.offset().top))
            + ' | height:' + (parseInt($shopItemsList.height()))
            + ' | offset:' + (parseInt($shopItemsList.offset().top))
        );*/
        //console.log('scroll',arguments);
        if (
            (parseInt($(document).scrollTop()) + parseInt($(window).height()) + 300)
            >=
            (parseInt($shopItemsList.height()) + parseInt($shopItemsList.offset().top))
            &&
            !isLoading
        ) {
            $(document).trigger('need-more-items');
        }
    });

    $(document).on('start-loading-items', function () {
        isLoading = true;
    }).on('stop-loading-items', function () {
        isLoading = false;
    });
});

// Autocomplete */
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function () {
                this.request();
            });

            // Blur
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function (event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }

            // Show
            this.show = function () {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu').show();
            }

            // Hide
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }

            // Request
            this.request = function () {
                clearTimeout(this.timer);

                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function (json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            }

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

        });
    }
})(window.jQuery);
