<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="ltr" lang="{{ app()->getLocale() }}" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="ltr" lang="{{ app()->getLocale() }}" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="{{ app()->getLocale() }}">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Oz1.ru</title>
    <base href=""/>
    <meta name="description" content="Example of category description"/>

    <link href="{{ mix('css/stylesheet.css') }}" rel="stylesheet"/>

    {{-- <link href="{{ URL::asset('image/cart.png') }}" rel="icon"/> --}}
</head>
<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(54869911, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/54869911" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
    window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]);?>
</script>

<header id="header">
    {{--<div class="header-nav">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-nav">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 center-nav">
                    <div id="header-before">
                        <div id="ishiheaderblock" class="clearfix">
                            <div>{{__('layout.support_phone',['+7 (xxx) xxx xx xx'])}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-nav">
                </div>

            </div>
        </div>
    </div>--}}

    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="desktop-logo">
                    <div id="logo">
                        <a href="/">
                            <img src="image/logo.png" title="" alt="" class="img-responsive"/>
                        </a>
                    </div>
                </div>
                {{--<div id="_desktop_seach_widget">
                    <div id="search_widget" class="search-widget">
                        <div class="search-logo"></div>
                        <form>
                            <div id="search" class="input-group">
                                <span class="search-tag hidden-lg-down">{{__('layout.more_then_products',[1000])}}</span>
                                <input id="ajax-search-text" type="text" name="search" value=""
                                       placeholder="{{__('layout.search_our_catalogue')}}"
                                       class="form-control input-lg"/>
                                <span class="input-group-btn">
                                    <button id="ajax-search-btn" type="button" class="btn btn-default btn-lg">
                                    <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>


                        </form>
                    </div>
                </div> --}}
                <div class="desktop-custominfo">

                </div>
                <div id="mobile_top_menu_wrapper" class="hidden-lg hidden-md" style="display:none;">
                    <div id="top_menu_closer">
                        <i class="fa fa-close"></i>
                    </div>
                    <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="top_bottom_content clearfix">
        <div class="ishionebannerblock">
            <div class="bannerblock">
                <a href="/" class="ishi-customhover-fadeinnormal ">
                    <img src="image/banner-1470x100.png" alt="banner" class="img-responsive" />
                </a>
            </div>

        </div>
    </div>

    {{--
    <div class="nav-full-width other">
        <div class="container">
            <div class="row">


                <div id="_desktop_top_menu" class="menu js-top-menu hidden-xs hidden-sm open">
                    @if (!empty($categories))
                    <h2 class="home-title hidden-xs hidden-sm">{{__('layout.Categories')}}</h2>
                    <div class="wrapper">
                        <div class="line-menu half start"></div>
                        <div class="line-menu"></div>
                        <div class="line-menu half end"></div>
                    </div>
                    @include ('shop.categories',['categories'=>$categories])
                    @endif
                </div>

                <div id="menu-icon" class="menu-icon hidden-md hidden-lg">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>

                <div id="_mobile_cart"></div>
                <div id="_mobile_user_info"></div>
                <div id="_mobile_seach_widget"></div>
                <div id="_mobile_link_menu"></div>

            </div>
        </div>
    </div>
    --}}
</header>

@include ('shop.index',['pagination' => $pagination,'sorter' => $sorter,'items' => $items,'category' => $category,
            'categories' => $categories,'subCategories' => $subCategories,'breadcrumbs' => $breadcrumbs])

<div id="_mobile_column_left" class="container"></div>
<div id="_mobile_column_right" class="container"></div>

<div class="col-sm-12">
    <img src="image/category-img-1170x210.jpg"
         alt="Desktops" title="Desktops"
         class="img-responsive"/>
</div>

<footer id="footer" class="other-footer">
    <div class="footer-before">
        <div class="container">
            <div class="row">

            </div>
        </div>
    </div>

    <div class="footer-container">
        <div class="container">
            <?php /*
            <div class="row">
                <div class="block-contact col-lg-3 col-md-3 col-sm-12 footer-block">
                    <h5 class="hidden-sm hidden-xs">Store Information</h5>
                    <div class="footer-title clearfix hidden-lg hidden-md collapsed"
                         data-target="#contact-info-container" data-toggle="collapse">
                        <span class="h3">Store Information</span>
                        <span class="navbar-toggler collapse-icons">
                <i class="fa fa-angle-down add"></i>
                <i class="fa fa-angle-up remove"></i>
              </span>
                    </div>
                    <div id="contact-info-container" class="footer-contact collapse footer-dropdown">
                        <div class="block address">
                            <span class="icon"><i class="fa fa-map-marker"></i></span>
                            <div class="content">01 Demo Street.</div>
                        </div>
                        <div class="block phone">
                            <span class="icon phone"><i class="fa fa-phone"></i></span>
                            <div class="content">
                                <a href="/">+00 900 123456789</a>
                            </div>
                        </div>
                        <div class="block email">
                            <span class="icon"><i class="fa fa-envelope"></i></span>
                            <div class="content">
                                <a href="mailto:info@example.com">info@example.com</a>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="information col-lg-2 col-md-2 col-sm-12 footer-block">
                    <h5 class="hidden-sm hidden-xs">Information</h5>
                    <div class="footer-title clearfix  hidden-lg hidden-md collapsed"
                         data-target="#information-container" data-toggle="collapse">
                        <span class="h3">Information</span>
                        <span class="navbar-toggler collapse-icons">
              <i class="fa fa-angle-down add"></i>
              <i class="fa fa-angle-up remove"></i>
            </span>
                    </div>

                    <div id="information-container" class="collapse footer-dropdown">
                        <ul class="list-unstyled">
                            <li><a href="index.php%3Froute=information%252Finformation&amp;information_id=4.html">About
                                Us</a></li>
                            <li><a href="index.php%3Froute=information%252Finformation&amp;information_id=6.html">Delivery</a>
                            </li>
                            <li><a href="index.php%3Froute=information%252Finformation&amp;information_id=3.html">Privacy
                                Policy</a></li>
                            <li><a href="index.php%3Froute=information%252Finformation&amp;information_id=5.html">Terms
                                &amp; Conditions</a></li>
                            <li><a href="index.php%3Froute=information%252Finformation&amp;information_id=7.html">Information</a>
                            </li>
                        </ul>
                    </div>
                </section>
                <section class="Services col-lg-2 col-md-2 col-sm-12 footer-block">
                    <h5 class="hidden-sm hidden-xs">Customer Service</h5>
                    <div class="footer-title clearfix  hidden-lg hidden-md collapsed" data-target="#services-container"
                         data-toggle="collapse">
                        <span class="h3">Customer Service</span>
                        <span class="navbar-toggler collapse-icons">
              <i class="fa fa-angle-down add"></i>
              <i class="fa fa-angle-up remove"></i>
            </span>
                    </div>
                    <div id="services-container" class="collapse footer-dropdown">
                        <ul class="list-unstyled">
                            <li><a href="index.php%3Froute=account%252Freturn%252Fadd.html">Returns</a></li>
                        </ul>
                    </div>
                </section>
                <section class="extras col-lg-2 col-md-2 col-sm-12 footer-block">
                    <h5 class="hidden-sm hidden-xs">Extras</h5>
                    <div class="footer-title clearfix  hidden-lg hidden-md collapsed" data-target="#extras-container"
                         data-toggle="collapse">
                        <span class="h3">Extras</span>
                        <span class="navbar-toggler collapse-icons">
              <i class="fa fa-angle-down add"></i>
              <i class="fa fa-angle-up remove"></i>
            </span>
                    </div>
                    <div id="extras-container" class="collapse footer-dropdown">
                        <ul class="list-unstyled">
                            <li><a href="index.php%3Froute=product%252Fmanufacturer.html">Brands</a></li>
                            <li><a href="index.php%3Froute=account%252Fvoucher.html">Gift Certificates</a></li>
                            <li><a href="index.php%3Froute=information%252Fcontact.html">Contact Us</a></li>
                            <li><a href="index.php%3Froute=affiliate%252Flogin.html">Affiliate</a></li>
                            <li><a href="index.php%3Froute=product%252Fspecial.html">Specials</a></li>
                        </ul>
                    </div>
                </section>
                <section class="account col-lg-2 col-md-2 col-sm-12 footer-block">
                    <h5 class="hidden-sm hidden-xs">My Account</h5>
                    <div class="footer-title clearfix  hidden-lg hidden-md collapsed" data-target="#account-container"
                         data-toggle="collapse">
                        <span class="h3">My Account</span>
                        <span class="navbar-toggler collapse-icons">
              <i class="fa fa-angle-down add"></i>
              <i class="fa fa-angle-up remove"></i>
            </span>
                    </div>
                    <div id="account-container" class="collapse footer-dropdown">
                        <ul class="list-unstyled">
                            <li><a href="index.php%3Froute=account%252Faccount.html">My Account</a></li>
                            <li><a href="index.php%3Froute=account%252Forder.html">Order History</a></li>
                            <li><a href="index.php%3Froute=information%252Fsitemap.html">Site Map</a></li>
                            <li><a href="index.php%3Froute=account%252Fwishlist.html">Wish List</a></li>
                            <li><a href="index.php%3Froute=account%252Fnewsletter.html">Newsletter</a></li>
                        </ul>
                    </div>
                </section>
                <div class="footerbefore col-lg-3 col-md-3 col-sm-12">
                    <div class="block_newsletter">
                        <h5 class="hidden-sm hidden-xs">Newsletter</h5>
                        <div class="footer-title clearfix hidden-lg hidden-md collapsed" data-target="#boxes"
                             data-toggle="collapse">
                            <span class="h3">Newsletter</span>
                            <span class="navbar-toggler collapse-icons">
                <i class="fa fa-angle-down add"></i>
                <i class="fa fa-angle-up remove"></i>
              </span>
                        </div>
                        <div id="boxes" class="newletter-container collapse footer-dropdown">
                            <div style="" id="dialog" class="window">
                                <div class="box">
                                    <div id="newsletter-container" class="box-content newleter-content">
                                        <div class="newsletter_text">
                                            <p class="block-newsletter-label">Sign up Latest Offer And Promotion</p>
                                        </div>
                                        <div id="frm_subscribe" class="newsletter_form">
                                            <form name="subscribe" id="subscribe">
                                                <input type="text" class="text-email" placeholder="Your email address"
                                                       value="" name="subscribe_email" id="subscribe_email">
                                                <input type="hidden" class="text-email" placeholder="Your email address"
                                                       value="" name="subscribe_name" id="subscribe_name"/>
                                                <a class="button btn-submit"
                                                   onclick="email_subscribe()"><span>Subscribe</span></a>
                                                <div id="notification-normal"></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box -->
                    <script type="text/javascript">
                        function email_subscribe() {
                            $.ajax({
                                type: 'post',
                                url: 'index.php?route=extension/module/newslettersubscribe/subscribe',
                                dataType: 'html',
                                data: $("#subscribe").serialize(),
                                success: function (html) {
                                    try {

                                        eval(html);

                                    } catch (e) {
                                    }

                                }
                            });
                        }

                        function email_unsubscribe() {
                            $.ajax({
                                type: 'post',
                                url: 'index.php?route=extension/module/newslettersubscribe/unsubscribe',
                                dataType: 'html',
                                data: $("#subscribe").serialize(),
                                success: function (html) {
                                    try {

                                        eval(html);

                                    } catch (e) {
                                    }
                                }
                            });
                            $('html, body').delay(1500).animate({scrollTop: 0}, 'slow');
                        }
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#subscribe_email').keypress(function (e) {
                                if (e.which == 13) {
                                    e.preventDefault();
                                    email_subscribe();
                                }
                                var name = $(this).val();
                                $('#subscribe_name').val(name);
                            });
                            $('#subscribe_email').change(function () {
                                var name = $(this).val();
                                $('#subscribe_name').val(name);
                            });

                        });
                    </script>

                </div>
            </div>

*/ ?>
        </div>
    </div>

    <div class="footer-after">
        <div class="container">
            <div class="row">
                <?php /* */ ?>
                <div class="col-lg-12 col-md-12 col-xs-12 ">
                    <div class="ishiservicesblock">
                        <div class="ishiservices">
                            <div class="services support col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a href="/">
                                    <div class="service-img"></div>
                                    <div class="service-block">
                                        <div class="service-title">FREE DELIVERY</div>
                                        <div class="service-desc">Lorem ipsum dolor sit consect.</div>
                                    </div>
                                </a>
                            </div>
                            <div class="services saving col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a href="/">
                                    <div class="service-img"></div>
                                    <div class="service-block">
                                        <div class="service-title">GIFT VOUCHER</div>
                                        <div class="service-desc">Lorem ipsum dolor sit consect.</div>
                                    </div>
                                </a>
                            </div>
                            <div class="services delivery col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a href="/">
                                    <div class="service-img"></div>
                                    <div class="service-block">
                                        <div class="service-title">GREAT SAVING</div>
                                        <div class="service-desc">Lorem ipsum dolor sit consect.</div>
                                    </div>
                                </a>
                            </div>
                            <div class="services gift col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a href="/">
                                    <div class="service-img"></div>
                                    <div class="service-block">
                                        <div class="service-title">24X7 SUPPORT</div>
                                        <div class="service-desc">Lorem ipsum dolor sit consect.</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
<?php /*
                <p class="footer-aftertext col-lg-4 col-md-4 col-sm-12 col-xs-12">Powered By <a
                        href="http://www.opencart.com">OpenCart</a> Demo Store &copy; 2019</p>
*/ ?>

                <div class="block-social col-lg-4 col-md-4 col-sm-12">
                    <ul>
                        <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="youtube"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#" class="googleplus"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>

                <div id="ishipaymentblock0" class="ishipaymentblock col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="paymentblock">
                        <img src="image/payment/1-44x30.png" alt="payment-1" class="img-responsive"/>
                    </div>
                    <div class="paymentblock">
                        <img src="image/payment/2-44x30.png" alt="payment-2" class="img-responsive"/>
                    </div>
                    <div class="paymentblock">
                        <img src="image/payment/3-44x30.png" alt="payment-3" class="img-responsive"/>
                    </div>
                    <div class="paymentblock">
                        <img src="image/payment/4-44x30.png" alt="payment-4" class="img-responsive"/>
                    </div>
                    <div class="paymentblock">
                        <img src="image/payment/5-44x30.png" alt="payment-5" class="img-responsive"/>
                    </div>
                    <div class="paymentblock">
                        <img src="image/payment/6-44x30.png" alt="payment-6" class="img-responsive"/>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <a id="slidetop" href="#"></a>
</footer>

<script src="{{ mix('/js/app.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $("#_desktop_top_menu").click(function () {
        $("#top-menu").slideToggle();
        $('.wrapper').toggleClass('open');
    });
</script>

<!-- ======= Quick view JS ========= -->
<script>

    function quickbox() {
        if ($(window).width() > 767) {
            $('.quickview-button').magnificPopup({
                type: 'ajax',
                delegate: 'a',
                preloader: true,
                tLoading: 'Загрузка ...',
            });
        }
    }

    jQuery(document).ready(function () {
        quickbox();
    });
    jQuery(window).resize(function () {
        quickbox();
    });

</script>

{{-- <script>
    (function () {
        document.getElementById('ajax-search-text').addEventListener('keypress', function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                document.getElementById('ajax-search-btn').click();
            }
        });
    }());
</script> --}}

<script>
    var __instanceNameItemsListLoader = 'ItemsListLoader';

    (function ($) {
        'use strict';

        var ItemsListLoader = function (container, options) {
            return this.initialize(container, options);
        };

        ItemsListLoader.prototype = {
            options: {},
            defaults: {
                stlLoader: '.items-list-loader',
                stlAnimation: '.items-list-loader .load-animation',
                sltContent: '.product-list-js',
                sltPaginationInfo: '.category-pagination',
                sltPagination: '.pagination'
            },
            $container: null,
            $animation: null,
            $list: null,
            $paginationInfo: null,
            $pagination: null,
            initialize: function (container, options) {
                this.$container = $(container);
                this
                    .setData()
                    .setOptions(options)
                    .build()
                    .events();
                return this;
            },
            setData: function () {
                this.$container.data(__instanceNameItemsListLoader, this);
                return this;
            },
            setOptions: function (opts) {
                this.options = $.extend(true, {}, this.defaults, opts);
                return this;
            },
            build: function () {
                this.$animation = $(this.options.stlAnimation);
                this.$list = $(this.options.sltContent);
                this.$paginationInfo = $(this.options.sltPaginationInfo);
                this.$pagination = $(this.options.sltPagination);
                return this;
            },
            events: function () {
                var self = this;
                this.$container.on('click', function(){return self.onclick();});
                $(document).on('need-more-items', function(){return self.onclick();});
            },
            onclick: function () {

                if (this.$container.attr('disabled'))
                    return false;

                $(document).trigger('start-loading-items',this);
                const self = this;

                this.showAnimation();

                $.ajax(this.$container.attr('href')).success(function(data) {
                    try {
                        self.updateByHtml(data);
                    } catch (e) {
                        console.error(e);
                    }
                    $(document).trigger('success-loading-items',self);
                }).fail(function() {
                    console.log(arguments);
                    $(document).trigger('fail-loading-items',self);
                }).always(function() {
                    self.hideAnimation();
                    $(document).trigger('stop-loading-items',self);
                });

                return false;
            },
            showAnimation: function() {
                this.$animation.css('display','inline-block');
                this.$container.attr('disabled',true);
            },
            hideAnimation: function() {
                this.$animation.css('display','none');
                this.$container.attr('disabled',false);
            },
            updateByHtml: function(html) {
                let $container = $(html);
                let $loader = $container.find(this.options.stlLoader);
                let $list = $container.find(this.options.sltContent);
                let $paginationInfo = $container.find(this.options.sltPaginationInfo);
                let $pagination = $container.find(this.options.sltPagination);

                this.$list.append('<div class="page-delimiter"> Страница: '+($pagination.find('.active').html())+'</div>');
                this.$list.append($list.html());

                // еще костыль
                if (localStorage.getItem('display') == 'list') {
                    $('#list-view').trigger('click');
                    $('#list-view').addClass('active');
                } else {
                    $('#grid-view').trigger('click');
                    $('#grid-view').addClass('active');
                }
                quickbox();


                this.$paginationInfo.html($paginationInfo.html());
                this.$pagination.html($pagination.html());
                this.$container.attr('href',$loader.attr('href'));

            }
        };

        if (typeof($.fn.ItemsListLoaderController) === 'undefined') {
            $.fn.ItemsListLoaderController = function () {
                let options = {};
                if (
                    typeof(arguments[0]) !== 'undefined'
                    && arguments[0] != null
                    && arguments[0] instanceof Object !== false
                ) {
                    $.extend(options, arguments[0]);
                }

                return $(this).each(
                    function () {
                        let $this = $(this);
                        if ($this.data(__instanceNameItemsListLoader)) {
                            return $this.data(__instanceNameItemsListLoader);
                        } else {
                            return new ItemsListLoader($this, options);
                        }
                    }
                );
            }
        }

        $(document).ready(function () {
            if (typeof $.fn.ItemsListLoaderController !== 'undefined') {
                $('.items-list-loader').ItemsListLoaderController();
            }
        });

    })(jQuery);
</script>


</body>
</html>
