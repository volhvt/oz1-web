<?php
use App\ShopItem;
/** @var $item App\ShopItem */

?>


<div class="quickview">
    <div class="quickview-container">

        <div id="content" class="productpage-quickview">

            <div class="">
                <div class="col-sm-6 col-xs-12 product-left">
                    <div class="product-info">

                        <div class="left product-image thumbnails">

                            <div class="image">
                                <a class="thumbnail" title="{{$item->name}}">
                                    <img id="czzoom" src="{{$item->imageUrlH400}}"
                                         data-zoom-image="{{$item->picture}}"
                                         title="{{$item->name}}" alt="{{$item->name}}" />
                                </a>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="col-sm-6 col-xs-12 product-right">
                    <h3 class="product-title">{{$item->name}}</h3>

                    <div class="product-prices-block">
                    <ul class="list-unstyled price">

                        <li>
                            <h3 class="special-price">{{$item->price}} руб.</h3>
                        </li>
                        @if ($item->price_old > 0)
                            <li><span class="old-price" style="text-decoration: line-through;">{{$item->price_old}} руб.</span></li>
                        @endif

                    </ul>
                    @if ($item->price_old > 0 && $item->price_old > $item->price)
                        <h4 class="product-discount-percent">
                            скидка: {{number_format($item->discount * 100 / $item->price_old, 0, '.', ' ')}}%
                        </h4>
                    @endif
                    </div>

                    <div class="product-buttons-block">
                        <a href="{{$item->url}}&partner=vk_smm" target="_blank" class="btn btn-danger btn-lg">
                            Посмотреть
                        </a>
                    </div>

                    {{-- <div class="rating-wrapper">

                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star off fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star off fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star off fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star off fa-stack-1x"></i></span>

                    </div>

                    <ul class="list-unstyled attr">
                        <li><span>Brand:</span> <a href="/">HTC</a></li>
                        <li><span>Product Code:</span> Product 1</li>
                        <li><span>Reward Points:</span> 400</li>
                        <li><span>Availability:</span> In Stock</li>
                    </ul> --}}

                    {!!$item->description!!}

                    {!!$item->content!!}

                </div>

            </div>
        </div>





    </div>
</div>
