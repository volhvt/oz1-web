<div id="product-category" class="container wrapper_container">
    <div class="row">

        <?php /*
        <div id="_desktop_column_left">
            <aside id="column-left" class="col-sm-3 hidden-xs">
                <div class="box">
                    <h2 class="page-title hidden-sm hidden-xs">
                        Categories
                    </h2>
                    <div class="block-title clearfix  hidden-lg hidden-md collapsed" data-target="#box-container"
                         data-toggle="collapse">
                        <span class="page-title">Categories</span>
                        <span class="navbar-toggler collapse-icons">
          <i class="fa fa-angle-down add"></i>
          <i class="fa fa-angle-up remove"></i>
        </span>
                    </div>
                    <div id="box-container" class="collapse data-toggler">
                        <ul class="category-top-menu">
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=70.html"
                                   class="list-group-item">Legal notice (1)</a>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=20.html"
                                   class="list-group-item active">Desktops (15)</a>
                                <span class="navbar-toggler collapse-icons" data-target="#childlist"
                                      data-toggle="collapse">
                        <i class="fa fa-angle-down add"></i>
                        <i class="fa fa-angle-up remove"></i>
                    </span>

                                <div id="childlist" class="collapse">
                                    <ul class="category-sub-menu">
                                        <li>

                                            <a href="index.php%3Froute=product%252Fcategory&amp;path=20_26.html"
                                               class="list-group-item">&nbsp;&nbsp;&nbsp;- Pc (4)</a>
                                        </li>
                                        <li>

                                            <a href="index.php%3Froute=product%252Fcategory&amp;path=20_27.html"
                                               class="list-group-item">&nbsp;&nbsp;&nbsp;- Mac (4)</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=17.html"
                                   class="list-group-item">Software (3)</a>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=57.html"
                                   class="list-group-item">Tablets (3)</a>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=25.html"
                                   class="list-group-item">Components (18)</a>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=24.html"
                                   class="list-group-item">Phones &amp; PDAs (4)</a>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=33.html"
                                   class="list-group-item">Cameras (7)</a>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=66.html"
                                   class="list-group-item">Television (3)</a>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=67.html"
                                   class="list-group-item">Accessories (4)</a>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=68.html"
                                   class="list-group-item">Computer (4)</a>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=72.html"
                                   class="list-group-item">Woofer (4)</a>
                            </li>
                            <li>

                                <a href="index.php%3Froute=product%252Fcategory&amp;path=69.html"
                                   class="list-group-item">Delivery (4)</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="filterbox">
                    <div class="page-title hidden-sm hidden-xs">Refine Search</div>
                    <div class="block-title clearfix  hidden-lg hidden-md collapsed" data-target="#filterbox-container"
                         data-toggle="collapse">
                        <span class="page-title">Refine Search</span>
                        <span class="navbar-toggler collapse-icons">
      <i class="fa fa-angle-down add"></i>
      <i class="fa fa-angle-up remove"></i>
    </span>
                    </div>
                    <div id="filterbox-container" class="collapse data-toggler">
                        <div class="list-group-filter">

                            <a class="list-group-item group-name">Color</a>
                            <div class="list-group-item">
                                <div id="filter-group1">
                                    <div class="checkbox">
                                        <label> <input type="checkbox" name="filter[]" value="1"/>
                                            Red (11)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label> <input type="checkbox" name="filter[]" value="2"/>
                                            Green (12)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label> <input type="checkbox" name="filter[]" value="3"/>
                                            Pink (7)
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <a class="list-group-item group-name">Size</a>
                            <div class="list-group-item">
                                <div id="filter-group2">
                                    <div class="checkbox">
                                        <label> <input type="checkbox" name="filter[]" value="4"/>
                                            L (12)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label> <input type="checkbox" name="filter[]" value="5"/>
                                            XL (12)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label> <input type="checkbox" name="filter[]" value="6"/>
                                            XXL (12)
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-left">
                            <button type="button" id="button-filter" class="btn btn-primary">Refine Search</button>
                        </div>
                    </div>
                </div>
                <script type="text/javascript"><!--
                $('#button-filter').on('click', function () {
                    filter = [];

                    $('input[name^=\'filter\']:checked').each(function (element) {
                        filter.push(this.value);
                    });

                    location = 'http://demo.ishithemes.com/opencart/OPC012/index.php?route=product/category&path=20&filter=' + filter.join(',');
                });
                //--></script>

                <section class="featured-products clearfix">
                    <h3 class="page-title hidden-sm hidden-xs">
                        Bestsellers
                    </h3>
                    <div class="block-title clearfix  hidden-lg hidden-md collapsed" data-target="#bestseller-container"
                         data-toggle="collapse">
                        <span class="page-title">Bestsellers</span>
                        <span class="navbar-toggler collapse-icons">
      <i class="fa fa-angle-down add"></i>
      <i class="fa fa-angle-up remove"></i>
    </span>
                    </div>
                    <div id="bestseller-container" class="collapse data-toggler">
                        <div class="product-thumb transition">
                            <div class="image"><a
                                    href="index.php%3Froute=product%252Fproduct&amp;product_id=48.html"><img
                                    src="image/products/18-69x89.jpg" alt="iPod Classic"
                                    title="iPod Classic" class="img-responsive"/></a></div>
                            <div class="caption">
                                <h4><a href="index.php%3Froute=product%252Fproduct&amp;product_id=48.html">iPod
                                    Classic</a></h4>
                                <p class="description">More room to move.

                                    With 80GB or 160GB of storage and up to 40 hours of battery life, the new..</p>
                                <p class="price">
                                    $122.00
                                    <span class="price-tax">Ex Tax: $100.00</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-thumb transition">
                            <div class="image"><a
                                    href="index.php%3Froute=product%252Fproduct&amp;product_id=34.html"><img
                                    src="image/products/8-69x89.jpg" alt="iPod Shuffle"
                                    title="iPod Shuffle" class="img-responsive"/></a></div>
                            <div class="caption">
                                <h4><a href="index.php%3Froute=product%252Fproduct&amp;product_id=34.html">iPod
                                    Shuffle</a></h4>
                                <p class="description">Born to be worn.

                                    Clip on the worlds most wearable music player and take up to 240 songs with y..</p>
                                <p class="price">
                                    $122.00
                                    <span class="price-tax">Ex Tax: $100.00</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-thumb transition">
                            <div class="image"><a
                                    href="index.php%3Froute=product%252Fproduct&amp;product_id=40.html"><img
                                    src="image/products/5-69x89.jpg" alt="iPhone" title="iPhone"
                                    class="img-responsive"/></a></div>
                            <div class="caption">
                                <h4><a href="index.php%3Froute=product%252Fproduct&amp;product_id=40.html">iPhone</a>
                                </h4>
                                <p class="description">iPhone is a revolutionary new mobile phone that allows you to
                                    make a call by simply tapping a name o..</p>
                                <p class="price">
                                    $123.20
                                    <span class="price-tax">Ex Tax: $101.00</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-thumb transition">
                            <div class="image"><a
                                    href="index.php%3Froute=product%252Fproduct&amp;product_id=28.html"><img
                                    src="image/products/3-69x89.jpg" alt="HTC Touch HD"
                                    title="HTC Touch HD" class="img-responsive"/></a></div>
                            <div class="caption">
                                <h4><a href="index.php%3Froute=product%252Fproduct&amp;product_id=28.html">HTC Touch
                                    HD</a></h4>
                                <p class="description">HTC Touch - in High Definition. Watch music videos and streaming
                                    content in awe-inspiring high defin..</p>
                                <p class="price">
                                    <span class="price-new">$26.00</span> <span class="price-old">$122.00</span>
                                    <span class="price-tax">Ex Tax: $20.00</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="ishileftbanner">
                    <div class="bannerblock">
                        <a href="index.html#" class="ishi-customhover-fadeoutcenter">
                            <img src="image/left-banner-270x350.png" alt="Left Banner"
                                 class="img-responsive"/>
                        </a>
                    </div>
                </div>
                <section class="featured-products clearfix">
                    <h3 class="page-title hidden-sm hidden-xs">
                        Latest
                    </h3>
                    <div class="block-title clearfix  hidden-lg hidden-md collapsed" data-target="#latest-container"
                         data-toggle="collapse">
                        <span class="page-title">Latest</span>
                        <span class="navbar-toggler collapse-icons">
      <i class="fa fa-angle-down add"></i>
      <i class="fa fa-angle-up remove"></i>
    </span>
                    </div>
                    <div id="latest-container" class="collapse data-toggler">
                        <div class="product-thumb transition">
                            <div class="image"><a
                                    href="index.php%3Froute=product%252Fproduct&amp;product_id=49.html"><img
                                    src="image/products/19-69x89.jpg" alt="Samsung Galaxy Tab 10.1"
                                    title="Samsung Galaxy Tab 10.1" class="img-responsive"/></a></div>
                            <div class="caption">
                                <h4><a href="index.php%3Froute=product%252Fproduct&amp;product_id=49.html">Samsung
                                    Galaxy Tab 10.1</a></h4>
                                <p class="description">Samsung Galaxy Tab 10.1, is the world&rsquo;s thinnest tablet,
                                    measuring 8.6 mm thickness, running w..</p>
                                <p class="price">
                                    $241.99
                                    <span class="price-tax">Ex Tax: $199.99</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-thumb transition">
                            <div class="image"><a
                                    href="index.php%3Froute=product%252Fproduct&amp;product_id=48.html"><img
                                    src="image/products/18-69x89.jpg" alt="iPod Classic"
                                    title="iPod Classic" class="img-responsive"/></a></div>
                            <div class="caption">
                                <h4><a href="index.php%3Froute=product%252Fproduct&amp;product_id=48.html">iPod
                                    Classic</a></h4>
                                <p class="description">More room to move.

                                    With 80GB or 160GB of storage and up to 40 hours of battery life, the new..</p>
                                <p class="price">
                                    $122.00
                                    <span class="price-tax">Ex Tax: $100.00</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-thumb transition">
                            <div class="image"><a
                                    href="index.php%3Froute=product%252Fproduct&amp;product_id=47.html"><img
                                    src="image/products/17-69x89.jpg" alt="HP LP3065" title="HP LP3065"
                                    class="img-responsive"/></a></div>
                            <div class="caption">
                                <h4><a href="index.php%3Froute=product%252Fproduct&amp;product_id=47.html">HP LP3065</a>
                                </h4>
                                <p class="description">Stop your co-workers in their tracks with the stunning new
                                    30-inch diagonal HP LP3065 Flat Panel Mon..</p>
                                <p class="price">
                                    <span class="price-new">$26.00</span> <span class="price-old">$122.00</span>
                                    <span class="price-tax">Ex Tax: $20.00</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-thumb transition">
                            <div class="image"><a
                                    href="index.php%3Froute=product%252Fproduct&amp;product_id=46.html"><img
                                    src="image/products/4-69x89.jpg" alt="Sony VAIO" title="Sony VAIO"
                                    class="img-responsive"/></a></div>
                            <div class="caption">
                                <h4><a href="index.php%3Froute=product%252Fproduct&amp;product_id=46.html">Sony VAIO</a>
                                </h4>
                                <p class="description">Unprecedented power. The next generation of processing technology
                                    has arrived. Built into the newest..</p>
                                <p class="price">
                                    $1,202.00
                                    <span class="price-tax">Ex Tax: $1,000.00</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </aside>
        </div>
*/ ?>

        <div id="content" class="">

            <div class="products">

                @include ('shop.groups-plate',['categories'=>$subCategories,'category'=>$category,'breadcrumbs'=>$breadcrumbs])

                <div class="product-list-top">
                    <div class="row">

                        <div class="btn-list-grid">
                            <div class="btn-group btn-group-sm">
                                <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip"
                                        title="{{__('layout.Grid')}}"><i class="fa fa-th"></i></button>
                                <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip"
                                        title="{{__('layout.List')}}"><i class="fa fa-th-list"></i></button>
                            </div>
                        </div>

                        <div class="pagination-right">
                            @include('shop/sort-by',['pagination'=>$pagination,'sorter'=>$sorter])
                            @include('shop/per-page',['pagination'=>$pagination,'sorter'=>$sorter])
                        </div>
                    </div>
                </div>
                <div class="row product-list-js">
                    @foreach( $pagination as $item)
                        @include('shop/item-in-list',['item'=>$item])
                    @endforeach
                </div>

                {{ $pagination->links('shop/paginate') }}

            </div>
        </div>
    </div>
</div>