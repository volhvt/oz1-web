<?php
use App\ShopItem;
/** @var $item App\ShopItem */

?>

<div class="product-layout product-list col-xs-12">
    <div class="product-thumb">
        <div class="image">

            <a href="{{$item->url}}&partner=vk_smm" target="_blank">
                <?php /*
                <img src="{{$item->picture}}" alt="{{$item->name}}"
                     title="{{$item->name}}" class="img-responsive"/>


                <img class="product-img-extra img-responsive" alt="{{$item->name}}"
                     title="{{$item->name}}"
                     src="@if ($item->picture1){{$item->picture1}}@else{{$item->picture}}@endif"/>
                */ ?>
                <img src="{{$item->imageUrlH200}}" alt="{{$item->name}}"
                     title="{{$item->name}}" class="img-responsive"/>
                <img class="product-img-extra img-responsive" alt="{{$item->name}}"
                     title="{{$item->name}}"
                     src="{{$item->imageUrlH200}}"/>
            </a>

            {{-- <div class="rating">

                    <span class="fa fa-stack">
                      <i class="fa fa-star fa-stack-2x"></i>
                    </span>

            <span class="fa fa-stack">
                      <i class="fa fa-star fa-stack-2x"></i>
                    </span>

            <span class="fa fa-stack">
                      <i class="fa fa-star fa-stack-2x"></i>
                    </span>

            <span class="fa fa-stack">
                      <i class="fa fa-star-o fa-stack-2x"></i>
                    </span>

            <span class="fa fa-stack">
                      <i class="fa fa-star-o fa-stack-2x"></i>
                    </span>
            </div> --}}

            <div class="button-group">
                <div class="btn-quickview">
                    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
                        <a class="quickbox"
                           href="{{url('item',$item->id)}}">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <?php /*
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('42', '2');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
*/ ?>
            </div>
        </div>
        <div class="caption">
            <h4><a href="{{$item->url}}&partner=vk_smm" target="_blank">{{$item->name}}</a>
            </h4>
            <p class="description">{{$item->description}}</p>
            <p class="price">

                <span class="price-new"><i class="fas fa-ruble-sign"></i>{{$item->price}} руб.</span>
                @if ($item->price_old > 0)
                    <span class="price-old"><i class="fas fa-ruble-sign"></i>{{$item->price_old}} руб.</span>
                @endif
                @if ($item->price_old > 0)
                    <span class="discount-percent">
                        скидка: {{number_format($item->discount * 100 / $item->price_old, 0, '.', ' ')}}%
                    </span>
                @endif

            </p>

        </div>
    </div>
</div>

<?php /*
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=30.html">
<img src="image/cache/catalog/products/14-370x480.jpg" alt="Canon EOS 5D"
     title="Canon EOS 5D" class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="Canon EOS 5D"
     title="Canon EOS 5D" src="image/cache/catalog/products/19-370x480.jpg"/>
</a>
<div class="rating">

        <span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>
</div>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=30.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('30');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('30');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('30', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=30.html">Canon
    EOS 5D</a></h4>
<p class="description">Canon's press material for the EOS 5D states that it 'defines (a)
new D-SLR category', while we're n..</p>
<p class="price">
<span class="price-new">$98.00</span>
<span class="price-old">$122.00</span>

<span class="price-tax">Ex Tax: $80.00</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=47.html">
<img src="image/cache/catalog/products/17-370x480.jpg" alt="HP LP3065"
     title="HP LP3065" class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="HP LP3065" title="HP LP3065"
     src="image/cache/catalog/products/3-370x480.jpg"/>
</a>
<div class="rating">

        <span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>
</div>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=47.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('47');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('47');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('47', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=47.html">HP
    LP3065</a></h4>
<p class="description">Stop your co-workers in their tracks with the stunning new
30-inch diagonal HP LP3065 Flat Panel Mon..</p>
<p class="price">
<span class="price-new">$26.00</span>
<span class="price-old">$122.00</span>

<span class="price-tax">Ex Tax: $20.00</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=28.html">
<img src="image/cache/catalog/products/3-370x480.jpg" alt="HTC Touch HD"
     title="HTC Touch HD" class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="HTC Touch HD"
     title="HTC Touch HD" src="image/cache/catalog/products/5-370x480.jpg"/>
</a>
<div class="rating">

        <span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>
</div>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=28.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('28');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('28');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('28', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=28.html">HTC
    Touch HD</a></h4>
<p class="description">HTC Touch - in High Definition. Watch music videos and streaming
content in awe-inspiring high defin..</p>
<p class="price">
<span class="price-new">$26.00</span>
<span class="price-old">$122.00</span>

<span class="price-tax">Ex Tax: $20.00</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=40.html">
<img src="image/cache/catalog/products/5-370x480.jpg" alt="iPhone" title="iPhone"
     class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="iPhone" title="iPhone"
     src="image/cache/catalog/products/3-370x480.jpg"/>
</a>
<div class="rating">

        <span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>
</div>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=40.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('40');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('40');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('40', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=40.html">iPhone</a>
</h4>
<p class="description">iPhone is a revolutionary new mobile phone that allows you to
make a call by simply tapping a name o..</p>
<p class="price">
$123.20

<span class="price-tax">Ex Tax: $101.00</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=48.html">
<img src="image/cache/catalog/products/18-370x480.jpg" alt="iPod Classic"
     title="iPod Classic" class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="iPod Classic"
     title="iPod Classic" src="image/cache/catalog/products/4-370x480.jpg"/>
</a>
<div class="rating">

        <span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>
</div>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=48.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('48');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('48');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('48', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=48.html">iPod
    Classic</a></h4>
<p class="description">More room to move.

With 80GB or 160GB of storage and up to 40 hours of battery life, the new..</p>
<p class="price">
$122.00

<span class="price-tax">Ex Tax: $100.00</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=43.html">
<img src="image/cache/catalog/products/9-370x480.jpg" alt="MacBook" title="MacBook"
     class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="MacBook" title="MacBook"
     src="image/cache/catalog/products/10-370x480.jpg"/>
</a>
<div class="rating">

        <span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>
</div>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=43.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('43');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('43');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('43', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=43.html">MacBook</a>
</h4>
<p class="description">Intel Core 2 Duo processor

Powered by an Intel Core 2 Duo processor at speeds up to 2.16GHz, t..</p>
<p class="price">
$602.00

<span class="price-tax">Ex Tax: $500.00</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=44.html">
<img src="image/cache/catalog/products/20-370x480.jpg" alt="MacBook Air"
     title="MacBook Air" class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="MacBook Air" title="MacBook Air"
     src="image/cache/catalog/products/17-370x480.jpg"/>
</a>
<div class="rating">

        <span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>
</div>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=44.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('44');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('44');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('44', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=44.html">MacBook
    Air</a></h4>
<p class="description">MacBook Air is ultrathin, ultraportable, and ultra unlike
anything else. But you don&rsquo;t lose in..</p>
<p class="price">
$1,202.00

<span class="price-tax">Ex Tax: $1,000.00</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=31.html">
<img src="image/cache/catalog/products/14-370x480.jpg" alt="Nikon D300"
     title="Nikon D300" class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="Nikon D300" title="Nikon D300"
     src="image/cache/catalog/products/18-370x480.jpg"/>
</a>
<div class="rating">

        <span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>
</div>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=31.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('31');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('31');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('31', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=31.html">Nikon
    D300</a></h4>
<p class="description">Engineered with pro-level features and performance, the
12.3-effective-megapixel D300 combines brand..</p>
<p class="price">
$98.00

<span class="price-tax">Ex Tax: $80.00</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=29.html">
<img src="image/cache/catalog/products/2-370x480.jpg" alt="Palm Treo Pro"
     title="Palm Treo Pro" class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="Palm Treo Pro"
     title="Palm Treo Pro" src="image/cache/catalog/products/14-370x480.jpg"/>
</a>
<div class="rating">

        <span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>
</div>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=29.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('29');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('29');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('29', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=29.html">Palm
    Treo Pro</a></h4>
<p class="description">Redefine your workday with the Palm Treo Pro smartphone.
Perfectly balanced, you can respond to busi..</p>
<p class="price">
$337.99

<span class="price-tax">Ex Tax: $279.99</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=35.html">
<img src="image/cache/catalog/products/16-370x480.jpg" alt="Product 8"
     title="Product 8" class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="Product 8" title="Product 8"
     src="image/cache/catalog/products/8-370x480.jpg"/>
</a>
<div class="rating">

        <span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star fa-stack-2x"></i>
        </span>

<span class="fa fa-stack">
          <i class="fa fa-star-o fa-stack-2x"></i>
        </span>
</div>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=35.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('35');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('35');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('35', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=35.html">Product
    8</a></h4>
<p class="description">Product 8..</p>
<p class="price">
$122.00

<span class="price-tax">Ex Tax: $100.00</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=33.html">
<img src="image/cache/catalog/products/10-370x480.jpg"
     alt="Samsung SyncMaster 941BW" title="Samsung SyncMaster 941BW"
     class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="Samsung SyncMaster 941BW"
     title="Samsung SyncMaster 941BW"
     src="image/cache/catalog/products/2-370x480.jpg"/>
</a>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=33.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('33');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('33');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('33', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=33.html">Samsung
    SyncMaster 941BW</a></h4>
<p class="description">Imagine the advantages of going big without slowing down. The big
19&quot; 941BW monitor combines wi..</p>
<p class="price">
$242.00

<span class="price-tax">Ex Tax: $200.00</span>

</p>

</div>
</div>
</div>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=46.html">
<img src="image/cache/catalog/products/4-370x480.jpg" alt="Sony VAIO"
     title="Sony VAIO" class="img-responsive"/>
<img class="product-img-extra img-responsive" alt="Sony VAIO" title="Sony VAIO"
     src="image/cache/catalog/products/20-370x480.jpg"/>
</a>

<div class="button-group">
<div class="btn-quickview">
    <div class="quickview-button button" data-toggle="tooltip" title=" Quick View">
        <a class="quickbox"
           href="index.php%3Froute=product%252Fquick_view&amp;product_id=46.html">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="btn-wishlist">
    <button type="button" data-toggle="tooltip" title="Add to Wish List"
            onclick="wishlist.add('46');"><i class="fa fa-heart"></i></button>
</div>
<div class="btn-compare">
    <button type="button" data-toggle="tooltip" title="Compare this Product"
            onclick="compare.add('46');"><i class="fa fa-exchange"></i></button>
</div>
<div class="btn-cart">
    <button type="button" onclick="cart.add('46', '1');"><i
                class="fa fa-shopping-cart"></i> <span
                class="lblcart">Add to Cart</span></button>
</div>
</div>
</div>
<div class="caption">
<h4><a href="index.php%3Froute=product%252Fproduct&amp;path=20&amp;product_id=46.html">Sony
    VAIO</a></h4>
<p class="description">Unprecedented power. The next generation of processing technology
has arrived. Built into the newest..</p>
<p class="price">
$1,202.00

<span class="price-tax">Ex Tax: $1,000.00</span>

</p>

</div>
</div>
</div>
*/ ?>