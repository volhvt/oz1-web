{{-- @if (!empty($category))--}}
    <div class="category_banner">
        {{-- <h1 class="page-title">{{$category->name}}</h1>--}}

        <div class="row">
            {{-- <div class="col-sm-12 category_img">
                <img src="image/category-img-1170x210.jpg"
                     alt="Desktops" title="Desktops"
                     class="img-responsive"/>
            </div>

            <div class="col-sm-12 category_description">
                <p>Example of category description text</p>
            </div> --}}

        </div>
    </div>
{{-- @endif --}}

<div class="breadcrumb-container">
    @if (!empty($category))
        <h1 class="page-title">{{$category->name}}</h1>
    @else
        <h1 class="page-title">{{__('layout.Categories')}}</h1>
    @endif

    @if($breadcrumbs)
    <ul class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        @foreach ($breadcrumbs as $breadcrumb)
        <li><a href="{{url('/',$breadcrumb->id)}}">{{$breadcrumb->name}}</a></li>
        @endforeach
    </ul>
    @endif
</div>

<div class="refine-search">
    <div class="row">
        <div class="col-sm-12 category-list">
            <ul>

                @foreach ($categories as $category)
                <li><a href="{{$category->id}}">{{$category->name}}</a></li>
                @endforeach

            </ul>
        </div>
    </div>
</div>