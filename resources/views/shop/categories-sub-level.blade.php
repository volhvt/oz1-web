@if(count($categories)>0)

<span class="pull-xs-right hidden-md hidden-lg">
  <span data-target="#top_sub_menu_{{$parent->id}}" data-toggle="collapse" class="navbar-toggler collapse-icons">
    <i class="fa fa-angle-down add"></i>
    <i class="fa fa-angle-up remove"></i>
  </span>
</span>

<div id="top_sub_menu_{{$parent->id}}" class="dropdown-menu popover sub-menu collapse">

    @foreach($categories as $category)

        <ul class="list-unstyled childs_1 category_dropdownmenu  multiple-dropdown-menu "
            data-depth="1">

            <li class="category dropdown sub-category">
                <a class="dropdown-item dropdown-submenu" href="{{$category->id}}">{{$category->name}}</a>
                @if (count($category->children) > 0)
                <span class="pull-xs-right hidden-md hidden-lg">
                    <span data-target="#top_sub_menu_{{$category->id}}" data-toggle="collapse"
                          class="navbar-toggler collapse-icons">
                      <i class="fa fa-angle-down add"></i>
                      <i class="fa fa-angle-up remove"></i>
                    </span>
                </span>
                <div id="top_sub_menu_{{$category->id}}" class="dropdown-inner collapse">
                    <ul class="list-unstyled childs_2 top-menu" data-depth="2">
                        @foreach($category->children as $subCategory)
                        <li class="category">
                            <a class="dropdown-item"
                               href="{{$subCategory->id}}">{{$subCategory->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </li>
        </ul>

    @endforeach

</div>

@endif