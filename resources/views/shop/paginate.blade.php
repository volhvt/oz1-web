@if ($paginator->hasPages())
<div>

    <a class="btn btn-block btn-success items-list-loader" data-content="product-list-js"
            data-pagination-info="category-pagination" data-pagination="pagination"
            href="{{$paginator->nextPageUrl()}}" rel="next" style="@if (!$paginator->hasMorePages())display:none;@endif"
    ><i class="fa fa-refresh fa-spin fa-3x fa-fw load-animation"></i>{{__('layout.Load more')}}...</a>

</div>

<div class="category-pagination">
    <div class="row">
        <div class="col-sm-6 col-xs-12 text-left pagination-desc">{{__('layout.Showing_to_of_Pages',
        [
            (($paginator->currentPage()-1)*$paginator->perPage())+1,
            $paginator->currentPage()*$paginator->perPage(),$paginator->total(),
            $paginator->lastPage()
        ])}}</div>
        <div class="col-sm-6 col-xs-12 text-right"></div>
    </div>
</div>

    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled"><span>&laquo;</span></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><span>{{ $page }}</span></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>
        @else
            <li class="disabled"><span>&raquo;</span></li>
        @endif
    </ul>
@endif