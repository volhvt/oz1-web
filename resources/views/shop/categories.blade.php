<ul id="top-menu" class="top-menu" data-depth="0">

    @foreach ($categories as $category)
    <li class="@if (count($category->children) > 0) top_level_category dropdown @else maintitle @endif">
        <a class="dropdown-item" href="{{$category->id}}">{{$category->name}}
            @if (count($category->children) > 0)
            <i class="fa fa-angle-right add hidden-xs hidden-sm"></i>
            @endif
        </a>
        @include ('shop/categories-sub-level',['categories'=>$category->children,'parent'=>$category])
    </li>
    @endforeach

</ul>