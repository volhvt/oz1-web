<div class="show-wrapper">
    <div class="col-md-1 text-right show">
        <label class="control-label" for="input-limit">{{__('layout.Show')}}:</label>
    </div>
    <div class="col-md-2 text-right limit">
        <div class="select-wrapper">
            <select id="input-limit" class="form-control" onchange="location = this.value;">

                @for ($i=50;$i<50*6;$i+=50)
                <option value="?limit={{$i}}"@if($i==$pagination->perPage())selected="selected"@endif>
                    {{$i}}
                </option>
                @endfor

            </select>
        </div>
    </div>
</div>