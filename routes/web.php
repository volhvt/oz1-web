<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Controller@index');
Route::get('/{categoryId}','Controller@index');
Route::get('/item/{itemId}','Controller@item');
Route::get('/crop-image/{h}/{w}/{image}', 'ImageController@resize')
    ->where('image', '.*');
