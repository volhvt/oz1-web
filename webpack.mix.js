let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('resources/assets/js/bootstrap/fonts/*','public/fonts/vendor/bootstrap-sass/bootstrap');
mix.copy('resources/assets/css/font-awesome/fonts/*','public/fonts/vendor/font-awesome');
mix.js([
    'resources/assets/js/app.js'
], 'public/js').version();
mix.sass('resources/assets/sass/app.scss', 'resources/assets/css/app.css');
mix.styles([
    'resources/assets/css/fonts.css',
    'resources/assets/css/font-awesome/css/font-awesome.min.css',
    'resources/assets/css/app.css',
    'resources/assets/js/bootstrap/css/bootstrap.min.css',
    'resources/assets/js/jquery/magnific/magnific-popup.css',
    'resources/assets/css/stylesheet.css'
],'public/css/stylesheet.css').version();

